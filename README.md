# LOG-Data

[![Build Status](https://gitlab.com/logbruchkoebel/data/badges/master/build.svg)](https://gitlab.com/logbruchkoebel/data/builds)
[![Play Store](https://developer.android.com/images/brand/en_generic_rgb_wo_60.png)](https://play.google.com/store/apps/details?id=de.nico.log)

## Mitarbeiten

Wenn du beim Inhalt der App helfen willst, kannst du auf der 
[Webseite](https://log-web.de/) nach neuen PDF-Dateien suchen und einen
[Issue](https://gitlab.com/logbruchkoebel/data/issues) auf GitLab
öffnen.

## Wofür ist dieses Projekt?

Die [offizielle Android App](https://gitlab.com/logbruchkoebel/android)
des Lichtenberg-Oberstufengymnasiums in Bruchköbel bekommt ihre Daten
aus diesem Repository.

## Wie füge ich ein neues Dokument zur Liste hinzu?

Die Dokumente werden in der Datei [`plans.json`](plans.json) aufgelistet.
Ein Eintrag sieht wie folgt aus:
```json
{
	"name": "Jahreskalender 17/18 (Stand 16.08.)",
	"filename": "kalender-17-18-stand-16-08",
	"url": "https://log-web.de/wp-content/uploads/2016/12/LOG-Termine-2017_18-aktuell_.pdf"
},
```
Beachte das Komma am Ende, welches gesetzt werden muss, wenn der Eintrag
nicht der letzte in der Liste ist.

Wenn Du die benötigten Berechtigungen hast, kannst Du die Liste in GitLab
aufrufen und auf "Edit" klicken.

## Wie kann ich ein erneutes Herunterladen eines Dokuments erzwingen?

Wenn es zum Beispiel eine neue Version des Kalenders gibt, kannst Du
durch Bearbeiten des Feldes `filename` ein erneutes Herunterladen erzwingen.
Ich habe in der Vergangenheit dann `-v1`, `-v2` und so weiter angehängt.

## TODO

- Data: [Issues](https://gitlab.com/logbruchkoebel/data/issues)
- Android: [Issues](https://gitlab.com/logbruchkoebel/android/issues)
- Upstream: [Issues](https://gitlab.com/asura/android/issues)

## Lizenz

Die `.json` Datei ist unter der MIT-Lizenz veröffentlicht. (Mehr
Informationen [hier](./LICENSE))

Der Inhalt der Pläne hingegen ist geistiges Eigentum des
Lichtenberg-Oberstufengymnasiums in Bruchköbel und eventuell unter einer
anderen Lizenz verfügbar.
